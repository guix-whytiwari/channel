(define-module (whytiwari packages browser-extensions)
  #:use-module (gnu build icecat-extension)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:))

(define translate-web-pages
  (package
    (name "translate-web-pages")
    (version "9.9.0.3")
    (source (origin
              (method url-fetch/zipbomb)
              (uri (string-append
                    "https://addons.mozilla.org/firefox/downloads/file/4165403/traduzir_paginas_web-9.9.0.30.xpi"))
              (sha256
               (base32
                "1zk4k760p8czh6l3r1j1g2rdiz2pqhyh5m14pjwxyzdmw9h44n4d"))))
    (build-system copy-build-system)
    (properties '((addon-id . "{036a55b4-5e72-4d05-a06c-cba2dfcc134a}")))
    (arguments
     `(#:install-plan '(("." ,(assq-ref properties 'addon-id)))))
    (home-page "https://addons.mozilla.org/en-US/firefox/addon/traduzir-paginas-web/")
    (synopsis "Translate your page in real time using Google or Yandex.")
    (description "What can this extension do?

    Your current page is translated without having to open new tabs.
    It is possible to change the translation language.
    You can select to automatically translate.
    To change the translation engine just touch the Google Translate icon. 

Why do you need to access your data on all the websites you visit?
To translate any website it is necessary to access and modify the text of the
web pages. And the extension can only do that, with that permission.

How are the pages translated?
The pages are translated using the Google or Yandex translation engine
(you choose).")
    (license license:gpl3+)))

(define-public translate-web-pages/icecat
  (make-icecat-extension translate-web-pages))
