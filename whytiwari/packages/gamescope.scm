(define-module (whytiwari packages gamescope)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages benchmark)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages stb)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:))

(define-public libliftoff
  (package
    (name "libliftoff")
    (version "0.4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.freedesktop.org/emersion/libliftoff")
             (commit (string-append "v" version))))
       (sha256
	(base32 "1ikjp638d655ycaqkdnzhb12d29kkbb3a46lqhbhsfc8vsqj3z1l"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags (list "--buildtype=release")))
    (native-inputs
     (list pkg-config))
    (inputs
     `(("libdrm", libdrm)))
    (home-page "")
    (synopsis "Lightweight KMS plane library")
    (description "libliftoff eases the use of KMS planes from userspace without standing in your way. 
                 Users create 'virtual planes called layers, set KMS properties on them, 
                 and libliftoff will pick planes for these layers if possible.")
    (license license:expat)))

(define-public vkroots
  (package
    (name "vkroots")
    (version "2675710")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Joshua-Ashton/vkroots")
             (commit "26757103dde8133bab432d172b8841df6bb48155")))
       (sha256
        (base32 "1gchc37d3d0k7mahgbn8h3k2zr0s9aiki3y04m8a1nafqha7xsvr"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags (list "--buildtype=release")
       #:phases
       (modify-phases
           %standard-phases
         (add-after 'unpack 'patch-build
		    (lambda _ (substitute* "meson.build"
				(("meson.override_dependency\\('vkroots', vkroots_dep\\)")
				 (string-append "meson.override_dependency('vkroots', vkroots_dep)" "\n"
						"install_headers('vkroots.h')" "\n"
						"pkgconfig = import('pkgconfig')" "\n"
						"pkgconfig.generate(" "\n"
						"    version: '" (version)  "',\n"
						"    filebase: meson.project_name()," "\n"
						"    name: meson.project_name()," "\n"
						"    description: 'Vulkan layers library'," "\n"
						"    subdirs: '.'" "\n"
						")" "\n"))))))))
    (native-inputs
     (list pkg-config))
    (home-page "https://github.com/Joshua-Ashton/vkroots")
    (synopsis "Framework for writing Vulkan layers")
    (description "vkroots is a framework for writing Vulkan layers that takes 
                  all the complexity and hastle away. It consists of a single C++20 header.")
    (license license:expat)))

(define-public libdisplay-info
  (package
   (name "libdisplay-info")
   (version "0.1.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://gitlab.freedesktop.org/emersion/libdisplay-info")
           (commit version)))
     (sha256
      (base32 "1ffq7w1ig1y44rrmkv1hvfjylzgq7f9nlnnsdgdv7pmcpfh45pgf"))))
   (build-system meson-build-system)
   (arguments
    `(#:tests? #f
      #:configure-flags (list "--buildtype=release")
      #:phases (modify-phases %standard-phases
			      (add-after 'unpack 'patch-deps
					 (lambda* (#:key inputs outputs #:allow-other-keys)
					   (let* ((out (assoc-ref outputs "out"))
						  (hwdata-pnp (assoc-ref inputs "hwdata:pnp")))
					     (substitute* "./meson.build"
							  (("hwdata_dep = dependency\\('hwdata'\\)")
							   "")
							  (("/usr/share/hwdata/pnp.ids")
							   (string-append hwdata-pnp "/share/hwdata/pnp.ids")))))))))
   (native-inputs
    (list cmake pkg-config))
   (inputs
    `(("hwdata:pnp", hwdata "pnp")
      ("libdrm", libdrm)
      ("coreutils", coreutils)
      ("python", python)))
   (home-page "")
   (synopsis "EDID and DisplayID library")
   (description " This library parse EDID and DisplayID and focus on:
 * Providing a set of high-level, easy-to-use, opinionated functions as well
   as low-level functions to access detailed information.
 * Simplicity and correctness over performance and resource usage.
 * Well-tested and fuzzed.
")
   (license license:expat)))

(define-public reshade-src
  (package
    (name "reshade-src")
    (version "9fdbea6")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Joshua-Ashton/reshade")
             (commit "9fdbea6892f9959fdc18095d035976c574b268b7")))
       (sha256
	(base32 "0wvrmjsszlsfy6p47i775x784xnsc130zvj18zjgx6fwq6amcf4i"))))
    (build-system copy-build-system)
    (home-page "")
    (synopsis "Lightweight KMS plane library")
    (description "libliftoff eases the use of KMS planes from userspace without standing in your way. 
                 Users create 'virtual planes called layers, set KMS properties on them, 
                 and libliftoff will pick planes for these layers if possible.")
    (license license:expat)))

(define-public spirv-headers-src
  (package
    (name "spirv-headers-src")
    (version "d790ced")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/SPIRV-Headers.git")
             (commit "d790ced752b5bfc06b6988baadef6eb2d16bdf96")))
       (sha256
	(base32 "1zzkqbqysiv52dk92i35gsyppnasln3d27b4rqv590zknk5g38is"))))
    (build-system copy-build-system)
    (home-page "")
    (synopsis "Lightweight KMS plane library")
    (description "libliftoff eases the use of KMS planes from userspace without standing in your way. 
                 Users create 'virtual planes called layers, set KMS properties on them, 
                 and libliftoff will pick planes for these layers if possible.")
    (license license:expat)))

(define stb
  ;; stb is a collection of libraries developed within the same repository.
  ;; When updating this, remember to change versions below as appropriate.
  (let ((commit "b42009b3b9d4ca35bc703f5310eedc74f584be58")
        (revision "2"))
    (package
      (name "stb")
      (home-page "https://github.com/nothings/stb")
      (version (git-version "0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (sha256
                 (base32
                  "1gmcjhmj62mfdscrsg2hv4j4j9v447y8zj3rbrm7mqn94cx73z1i"))
                (file-name (git-file-name name version))))
      (build-system gnu-build-system)
      (arguments
       `(#:modules ((ice-9 ftw)
                    (ice-9 regex)
                    (srfi srfi-26)
                    ,@%gnu-build-system-modules)
         #:phases (modify-phases %standard-phases
                    (delete 'configure)
                    (delete 'build)
                    (replace 'check
                      (lambda _
                        (invoke "make" "-C" "tests" "CC=gcc")))
                    (replace 'install
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let ((out (assoc-ref outputs "out"))
                              (files (make-regexp "\\.(c|h|md)$")))
                          (for-each (lambda (file)
                                      (install-file file out))
                                    (scandir "." (cut regexp-exec files <>)))
                          #t))))))
      (synopsis "Single file libraries for C/C++")
      (description
       "This package contains a variety of small independent libraries for
the C programming language.")
      ;; The user can choose either license.
      (license license:expat))))

(define (make-stb-header-package name version description)
  (package
   (inherit stb)
   (name name)
   (version version)
   (source #f)
   (inputs (list stb))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((stb (assoc-ref %build-inputs "stb"))
                        (lib (string-join (string-split ,name #\-) "_"))
                        (out (assoc-ref %outputs "out")))
                    (install-file (string-append stb "/" lib ".h")
                                  (string-append out "/include"))
                    #t))))
   (description description)))

(define-public stb-image-resize
  (make-stb-header-package
   "stb-image-resize" "2.01"
   "stb-image is a small and self-contained library for image loading or
decoding from file or memory.  A variety of formats are supported."))


(define-public gamescope
  (package
   (name "gamescope")
   (version "3.12.7")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/ValveSoftware/gamescope")
                  (commit "3.12.7")))
            (sha256
             (base32
	      "0q0mjfxy074924d0pavm99lzi9vfyvzvhswg9kszy5r82rv4x1dw"))
	    (patches (parameterize
		    ((%patch-path
		      (map (lambda (directory)
			     (string-append directory "/whytiwari/packages/patches"))
			   %load-path)))
		  (search-patches "gamescope-add-mouse-based-fullscreen-toggle.patch")))))
   (build-system meson-build-system)
   (arguments
    `(#:configure-flags (list "--buildtype=release")
      #:phases (modify-phases %standard-phases
			      (add-after 'unpack 'patch-deps
					 (lambda* (#:key inputs outputs #:allow-other-keys)
					   (let* ((out (assoc-ref outputs "out"))
						  (stb-image (string-append (assoc-ref inputs
										       "stb-image")
									    "/include"))
						  (hwdata-pnp (assoc-ref inputs "hwdata:pnp")))
					     (substitute* "./meson.build"
							  (("hwdata_dep = dependency\\('hwdata'\\)")
							   "")
							  (("stb_dep = dependency\\('stb'\\)")
							   (string-append
							    "stb_dep = declare_dependency(include_directories: '"
							    stb-image "')"))
							  (("'force_fallback_for=wlroots,libliftoff'")
							   "'force_fallback_for='")
							  (("format\\(hwdata_dep.get_variable\\('pkgdatadir'\\) / 'pnp.ids'\\)")
							   (string-append "format('" hwdata-pnp
									  "/share/hwdata/pnp.ids')")))
					     (substitute* "./src/meson.build"
							  (("stb_dep = dependency\\('stb'\\)")
							   (string-append
							    "stb_dep = declare_dependency(include_directories: '"
							    stb-image "')"))))))
			      (add-after 'patch-deps 'add-reshade-src
					 (lambda* (#:key inputs outputs #:allow-other-keys)
					   (let* ((out (assoc-ref outputs "out"))
						  (reshade-src (assoc-ref inputs "reshade-src")))
					     (copy-recursively reshade-src "./src/reshade/"))))
			      (add-after 'add-reshade-src 'add-spirv-headers-src
					 (lambda* (#:key inputs outputs #:allow-other-keys)
					   (let* ((out (assoc-ref outputs "out"))
						  (spirv-headers-src (assoc-ref inputs "spirv-headers-src")))
					     (copy-recursively spirv-headers-src "./thirdparty/SPIRV-Headers/")))))))
   (native-inputs (list cmake pkg-config))
   (inputs `(("coreutils" ,coreutils)
	     ("gcc-toolchain" ,gcc-toolchain-12)
             ("libx11" ,libx11)
             ("libxdamage" ,libxdamage)
             ("libxcomposite" ,libxcomposite)
             ("libxrender" ,libxrender)
             ("libxext" ,libxext)
             ("libxfixes" ,libxfixes)
             ("libxxf86vm" ,libxxf86vm)
             ("libxtst" ,libxtst)
             ("libxres" ,libxres)
             ("libxcb" ,libxcb)
	     ("libxcb" ,libxmu)
	     ("libxcb" ,libxt)
             ("libdrm" ,libdrm)
             ("vulkan-loader" ,vulkan-loader)
             ("wayland" ,wayland)
             ("wayland-protocols" ,wayland-protocols)
             ("libxkbcommon" ,libxkbcommon)
             ("libcap" ,libcap)
             ("sdl2" ,sdl2)
             ("pipewire" ,pipewire)
             ("glibc" ,glibc)
             ("hwdata:pnp" ,hwdata "pnp")
	     ("openvr" ,openvr)
             ("stb-image" ,stb-image)
             ("stb-image-write" ,stb-image-write)
	     ("stb-image-resize" ,stb-image-resize)
             ("vkroots" ,vkroots)
             ("wlroots" ,wlroots-0.16)
             ("libliftoff" ,libliftoff)
	     ("libdisplay-info" ,libdisplay-info)
             ("glslang" ,glslang)
	     ("glm" ,glm)
             ("vulkan-headers" ,vulkan-headers)
	     ("reshade-src" ,reshade-src)
	     ("benchmark" ,benchmark)
	     ("spirv-headers-src" ,spirv-headers-src)))
   (home-page "https://github.com/ValveSoftware/gamescope")
   (synopsis "Micro-compositor for video games on Wayland")
   (description "gamescope is the micro-compositor optimized for running 
video games on Wayland.")
   (license license:expat)))
