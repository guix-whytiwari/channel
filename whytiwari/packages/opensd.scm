(define-module (whytiwari packages opensd)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gcc)
  #:use-module ((guix licenses) #:prefix license:))

(define-public opensd
  (package
    (name "opensd")
    (version "0.49.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/open-sd/opensd")
             (commit "5173d11d6214ab0c5ae101b526c1ca03191f1cb5")))
       (sha256
        (base32 "1vcg1hpmk9rhqpwidwzfkjlmw5h71afczpfn4n758629za40arvg"))
       (patches (parameterize
		    ((%patch-path
		      (map (lambda (directory)
			     (string-append directory "/whytiwari/packages/patches"))
			   %load-path)))
		  (search-patches "opensd-add-force-feedback.patch")))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags
           #~(list (string-append "-DUDEV_RULE_DIR="
                                  #$output
                                  "/lib/udev/rules.d")
		   ;; "-DOPT_POSTINSTALL=FALSE"
                   )
           #:tests? #f)) ; no test suite
    (inputs
     (list gcc-12 glibc cmake))
    (home-page "https://open-sd.gitlab.io/opensd-docs")
    (synopsis "Userspace driver for the Steam Deck")
    (description
     "OpenSD is a highly-configurable userspace driver for the Steam Deck.")
    (license license:gpl3)))
