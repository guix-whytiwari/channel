(define-module (whytiwari packages linux)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages linux)
  #:use-module ((guix licenses) #:prefix license:))

(define-public wireplumber-sans-elogind
  (package
   (inherit wireplumber)
   (name "wireplumber-sans-elogind")
   (inputs (modify-inputs (package-inputs wireplumber)
                          (delete "elogind")))))
