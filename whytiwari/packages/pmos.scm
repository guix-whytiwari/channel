(define-module (whytiwari packages pmos)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module ((guix licenses) #:prefix license:))

(define-public pmbootstrap
  (package
    (name "pmbootstrap")
    (version "2.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/postmarketOS/pmbootstrap")
                    (commit version)))
              (sha256
               (base32
		"06yvhzl4xhkvq3pgjc9yi1brszxnagaxr6jxfyvyvxkhjp8zhrl1"))))
    (build-system python-build-system)
    (arguments
     (list #:tests? #f
           #:phases #~(modify-phases %standard-phases
                        (add-after 'install 'fix-paths
                          (lambda* _
                            (let ((git (string-append #$git "/bin/"))
                                  (procps (string-append #$procps "/bin"))
                                  (openssl (string-append #$openssl "/bin"))
                                  (sudo "/run/setuid-programs"))
                              (wrap-program (string-append #$output
                                                           "/bin/pmbootstrap")
                                            `("PATH" ":" suffix
                                              ,(list git procps openssl sudo))))))
                        (replace 'check
                          (lambda* (#:key tests? #:allow-other-keys)
                            (when tests?
                              (invoke "pytest")))))))
    (native-inputs (list python-pytest python-pyopenssl))
    (inputs (list git procps openssl sudo tar))
    (home-page "https://postmarketos.org")
    (synopsis "Build and flash tool for postmarketOS")
    (description
     "Bootstrap program that abstracts everything in chroots and therefore
basically runs on top of any Linux distribution. Features:
@enumerate
@item chroot setup (distro-independent QEMU user emulation
@item clean chroot shutdown (umount) and zapping
@item build software as packages
@item cross-compile all armhf-packages
@item effective caching out of the box (survives chroot zaps)
@item installation targets
@item flasher abstractions
@item logging
@item security
@end enumerate")
    (license license:gpl3+)))
