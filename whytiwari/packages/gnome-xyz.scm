(define-module (whytiwari packages gnome-xyz)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages web)
  #:use-module ((guix licenses) #:prefix license:))

(define-public adw-gtk3-theme
  (package
    (name "adw-gtk3-theme")
    (version "5.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/lassekongo83/adw-gtk3.git")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0w72qkgyzh261s961153nfnhnvj5q70xvr9d6i17bqwik4vzssk2"))))
    (build-system meson-build-system)
    (inputs
     (list gtk+ sassc))
    (home-page "https://github.com/lassekongo83/adw-gtk3")
    (synopsis "The theme from libadwaita ported to GTK-3")
    (description
     "adw-gtk3 theme is the libadwaita theme for GTK3.")
    (license license:lgpl2.1)))
