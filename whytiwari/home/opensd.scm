(define-module (whytiwari home opensd)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services xdg)
  #:use-module (gnu packages linux)
  #:use-module (gnu services configuration)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (whytiwari packages opensd)
  #:export (home-opensd-configuration
            home-opensd-service-type))

(define-configuration/no-serialization home-opensd-configuration
  (opensd
   (file-like opensd)
   "The OpenSD package to use."))

(define (home-opensd-shepherd-service config)
  (list (shepherd-service
	 (documentation "Userspace controller driver.")
	 (provision '(opensd))
	 (start #~(make-forkexec-constructor
		   (list #$(file-append
			    (home-opensd-configuration-opensd config)
			    "/bin/opensdd"))))
	 (stop #~(make-kill-destructor)))))

(define home-opensd-service-type
  (service-type
   (name 'opensd)
   (extensions
    (list (service-extension home-shepherd-service-type
                             home-opensd-shepherd-service)))
   (description
    "Start opensd service.")
   (default-value (home-opensd-configuration))))
