(define-module (whytiwari system linux)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages cpio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services linux)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (modremove-configuration
	    modremove-configuration?
	    modremove-configuration-record?
	    modremove-service-type))

(define-record-type* <modremove-configuration>
  modremove-configuration make-modremove-configuration
  modremove-configuration?
  (modules   modremove-configuration-modules ;list of strings
             (default (list "mt7921e" "mt7921_common"))))

(define (modremove-shepherd-service config)
  (list (shepherd-service
	 (provision '(modremove))
	 (documentation "Remove kernel modules.")
	 (requirement '(user-processes))
	 (start #~(const #t))
	 (stop #~(lambda _
                   (let ((rmmod #$(file-append kmod "/bin/rmmod")))
                     (zero? (system* rmmod "mt7921e" "mt7921_common"))))))))

(define modremove-service-type
  (service-type (name 'modremove)
                (extensions (list (service-extension shepherd-root-service-type
                                                     modremove-shepherd-service)))
		(default-value (modremove-configuration))
                (description
                 "Remove kernel modules from the kernel before shutdown")))
